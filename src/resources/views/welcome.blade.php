<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>IBAN validation</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
        <style>
            .rounded {
                border-radius:10px !important
            }
            a, a:hover {
                color:#f0f0f0;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <h1 class="text-center m-3">IBAN validation</h1>
            <form action="/" method="post" class="bg-secondary text-white p-3 rounded">
                @csrf

                <p class="font-weight-bold">Fill IBAN below:</p>
                <div class="row">
                    <div class="col-sm-2 font-weight-bold m-1 col-form-label">
                        <label for="iban">IBAN:</label>
                    </div>
                    <div class="col-sm-8 m-1">
                        <input type="text" name="iban" id="iban" placeholder="FR7630006000011234567890189" class="form-control form-control">
                    </div>
                    <div class="col-sm-1 m-1"><input type="submit" value="Validate" class="btn btn-primary"></div>
                </div>
            </form>
            <p class="text-center m-2 mb-0 font-weight-bold col-form-label-lg">OR</p>
            <form action="/" method="post" enctype="multipart/form-data" class="bg-secondary text-white p-3 rounded">
                @csrf

                <p class="font-weight-bold">Upload list of IBAN's:</p>
                <div class="row">
                    <div class="col-sm-2 font-weight-bold m-1 col-form-label">
                        <label for="customFile">IBAN's file:</label>
                    </div>
                    <div class="col-sm-8 m-1 custom-file">
                        <input type="file" class="custom-file-input" id="customFile" name="ibans">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                    <div class="col-sm-1 m-1"><input type="submit" value="Validate" class="btn btn-primary"></div>
                </div>
                <p class="font-italic mb-0 col-form-label-sm">Example: <a href="/download/ibans-example.txt" class="">ibans-example.txt</a></p>
            </form>
            @if (!empty($error))
                <div class="bg-warning text-white p-3 rounded mt-3 font-weight-bold">{{$error}}</div>
            @elseif (!empty($iban) && $validation == true)
                <div class="bg-success text-white p-3 rounded mt-3 font-weight-bold">{{$iban}} is correct</div>
            @elseif (!empty($iban) && $validation == false)
                <div class="bg-danger text-white p-3 rounded mt-3 font-weight-bold">{{$iban}} is incorrect</div>
            @elseif (!empty($file))
                <div class="bg-info text-white p-3 rounded mt-3 font-weight-bold">IBANS validation file can be downloaded here: <a href="/download/validated/{{$file}}" class="">{{$file}}</a></div>
            @endif

        </div>
    </body>
</html>
