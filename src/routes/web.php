<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','IbanCheck@Index');

Route::post('/','IbanCheck@Submit');

Route::get('/download/{filename?}', function($filename)
{
    $filePath = storage_path().'/app/public/' . $filename;

    if (!File::exists($filePath)) {
        abort(404);
    }

    return \Response::download($filePath);
});

Route::get('/download/validated/{filename?}', function($filename)
{
    $filePath = storage_path().'/app/public/validated/' . $filename;

    if (!File::exists($filePath)) {
        abort(404);
    }

    return \Response::download($filePath);
});

