<?php

namespace App\Http\Controllers;

use Storage;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;

class IbanCheck extends Controller
{
    private $viewData = [];

    /*
     * List of countries and IBAN lengths
     * https://www.nordea.com/en/our-services/cashmanagement/iban-validator-and-information/iban-countries/
     */
    private $countries = [
        'AL' => '28', 'DZ' => '24', 'AD' => '24', 'AO' => '25', 'AT' => '20', 'AZ' => '28', 'BH' => '22', 'BY' => '28',
        'BE' => '16', 'BJ' => '28', 'BA' => '20', 'BR' => '29', 'VG' => '24', 'BG' => '22', 'BF' => '27', 'BI' => '16',
        'CM' => '27', 'CV' => '25', 'CG' => '27', 'CR' => '21', 'HR' => '21', 'CY' => '28', 'CZ' => '24', 'DK' => '18',
        'DO' => '28', 'EG' => '27', 'EE' => '20', 'FO' => '18', 'FI' => '18', 'FR' => '27', 'GA' => '27', 'GE' => '22',
        'DE' => '22', 'GI' => '23', 'GR' => '27', 'GL' => '18', 'GT' => '28', 'GG' => '22', 'HU' => '28', 'IS' => '26',
        'IR' => '26', 'IQ' => '23', 'IE' => '22', 'IM' => '22', 'IL' => '23', 'IT' => '27', 'CI' => '28', 'JE' => '22',
        'JO' => '30', 'KZ' => '20', 'XK' => '20', 'KW' => '30', 'LV' => '21', 'LB' => '28', 'LI' => '21', 'LT' => '20',
        'LU' => '20', 'MK' => '19', 'MG' => '27', 'ML' => '28', 'MT' => '31', 'MR' => '27', 'MU' => '30', 'MD' => '24',
        'MC' => '27', 'ME' => '22', 'MZ' => '25', 'NL' => '18', 'NO' => '15', 'PK' => '24', 'PS' => '29', 'PL' => '28',
        'PT' => '25', 'QA' => '29', 'RO' => '24', 'LC' => '32', 'SM' => '27', 'ST' => '25', 'SA' => '24', 'SN' => '28',
        'RS' => '22', 'SC' => '31', 'SK' => '24', 'SI' => '19', 'ES' => '24', 'SE' => '24', 'CH' => '21', 'TL' => '23',
        'TN' => '24', 'TR' => '26', 'UA' => '29', 'AE' => '23', 'GB' => '22', 'VA' => '22'
    ];

    public function Index()
    {
        return view('welcome');
    }

    public function Submit(Request $request)
    {
        if (isset($request->post()['iban'])) {
            $this->viewData['iban']         = trim($request->post()['iban']);
            $this->viewData['validation']   = $this->ValidateIban($this->viewData['iban']);
        }

        if (empty($this->viewData) && $request->file('ibans')) {
            $progressFile = $this->ProgressFile($request->file('ibans'));

            if (!empty($progressFile)) {
                $this->viewData['file'] = $progressFile;
            }
        }

        if (empty($this->viewData)) {
            $this->viewData['error'] = 'Input data is empty or incorrect!';
        }

        return view('welcome', $this->viewData);
    }

    private function ValidateIban(string $iban) : bool
    {
        $iban = strtoupper($iban);
        $iban = str_replace(' ', '', $iban);

        // IBAN standard validation
        if (!preg_match('/^[A-Z]{2}[0-9]{2}[A-Z0-9]{1,30}$/', $iban)) {
            return false;
        }

        // Split IBAN
        $country = substr($iban, 0, 2);
        $checksum = intval(substr($iban, 2, 2));
        $account = substr($iban, 4);

        // IBAN country and length validation
        if (!isset($this->countries[$country])) {
            return false;
        }
        if (strlen($iban) != $this->countries[$country]) {
            return false;
        }

        // Convert IBAN to Numeric
        $lettersRange = range('A','Z');
        $splitIban = str_split($account . $country . $checksum);
        $convertedIban = '';
        foreach ($splitIban as $character) {
            if (!is_numeric($character)) {
                $character = array_search($character, $lettersRange) + 10;
            }
            $convertedIban .= $character;
        }

        // Split Numeric IBAN and validate with mod-97 method
        $modulus = null;
        for ($i = 0; $i < strlen($convertedIban); ++$i) {
            $len = 9 - strlen($modulus);

            $left = strlen($convertedIban) - $i;
            if ($len > $left) {
                $len = $left;
            }

            $numbers = $modulus . substr($convertedIban, $i, $len);

            $i = $i + $len - 1; // Set new $i value

            $modulus = $numbers % 97;
        }

        return $modulus == 1;
    }

    private function ProgressFile(Object $file) : string
    {
        // Check uploaded file size ant type
        if (!$file->getSize() || $file->getMimeType() != 'text/plain') {
            return '';
        }

        // Read file
        if (!$ibans = file($file->getRealPath())) {
            return '';
        }

        // Validate ibans and log to string
        $text = '';
        foreach ($ibans as $iban) {
            $iban = str_replace("\n", '', $iban);
            $validation = $this->ValidateIban($iban) ? 'true' : 'false';

            $text .= $iban . ';' . $validation . "\n";
        }

        $name = $file->getClientOriginalName();
        $ext = $file->getClientOriginalExtension();
        $newName = str_replace('.' . $ext, '.out', $name);

        Storage::disk('local')->put('public/validated/' . $newName, $text);

        return $newName;
    }
}
