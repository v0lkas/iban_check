# IBAN validation

## Easy installation (tested on Linux and Windows)
1. Download repository
2. Unzip "docker_conf.zip"
3. Go to "iban_check" folder and run from terminal: `./run.sh -install`

## Default installation
1. Download repository
2. Unzip "docker_conf.zip"
3. Go to "iban_check" folder and run these Docker commands in terminal:
4. docker build iban_check .
5. docker run -d -p PORT:80 -v DIRECTORY:/var/www --name iban_check iban_check
6. docker exec -it iban_check /bin/sh
7. cd /var/www
8. cp .env.example .env
9. composer install
10. php artisan key:generate
11. chmod -R 777 storage

Note: Installation process depends on your system and configuration, as Docker is working different on Windows Home, Windows Pro, Linux and other systems.

## After installation your project should be configured and started. You do not need to start it manually.

## Starting project (next times you use it)
Go to "iban_check" directory and run this command: `./run.sh -start`

## Stopping project (next times you use it)
Go to "iban_check" directory and run this command: `./run.sh -stop`

## Running project in browser (Linux)
Go to http://192.168.98.100

## Running project in browser (Windows)
Go to http://192.168.98.100:808 or your default Docker IP or host.
Your can find container IP with this command: `docker inspect iban_check`, default port is 808.